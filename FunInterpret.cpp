#include "FunInterpret.h"
#include <cstdlib>
#include <cmath>
#include <functional>

MiniLFunInterpret::MiniLFunInterpret(MiniL& miniL):
miniL(miniL)
{
}


int MiniLFunInterpret::visitFunction( MiniLParser::FunctionContext* ctx, std::vector<int> inputArgs)
{
  if(ctx->vars.size() != inputArgs.size()) 
    throw std::string("Fonction "+ctx->fun_name->getText()+" appellée avec un nombre d'arguments inconsistent.");
  memory.clear();
  for(size_t i=0 ;i < inputArgs.size();i++)
  {
    if(inputArgs[i]<0)throw std::string("N'utilisez que des entiers positifs en entrée");
    memory.declare(ctx->vars[i]->getText(), inputArgs[i]);
  }
  return_interrupt=false;
  path.clear();
  visitFunction(ctx);
  if(!return_interrupt)throw std::string("La fonction n'a pas atteint de return");
  return result;
}

std::vector<int> MiniLFunInterpret::getPath()
{
  return path;
}


antlrcpp::Any MiniLFunInterpret::visitFunction(MiniLParser::FunctionContext* ctx)
{
  visitCommand(ctx->command());
  return memory;
}

antlrcpp::Any MiniLFunInterpret::visitCommand(MiniLParser::CommandContext* ctx)
{
  if(return_interrupt)return antlrcpp::Any();
  path.push_back(ctx->getAltNumber());
  if(ctx->LOCAL())
  {
    //LOCAL VARIABLE (ATTR expression)?
    if(ctx->expression())
      memory.declare(ctx->VARIABLE()->getText(), static_cast<int>(visitExpression(ctx->expression())));
    else
      memory.declare(ctx->VARIABLE()->getText());
  }
  else if (ctx->ATTR()){
    memory.set(ctx->VARIABLE()->getText(),static_cast<int>(visitExpression(ctx->expression())));
        //| VARIABLE ATTR expression
  }
  else if (ctx->IF()){
    //IF condition THEN cmdTrue=command (ELSE cmdFalse=command)? END
    if(static_cast<bool>(visitCondition(ctx->condition())))
      visitCommand(ctx->cmdTrue);
    else if(ctx->cmdFalse)
      visitCommand(ctx->cmdFalse);
  }
  else if(ctx->WHILE()){
    //WHILE condition DO cmdWhile=command END
    while(static_cast<bool>(visitCondition(ctx->condition())))
    {
      visitCommand(ctx->cmdWhile);
      if(return_interrupt)return antlrcpp::Any();
    }
  }
  else if(ctx->PRINT()){
    // PRINT expression
    int res = static_cast<int>(visitExpression(ctx->expression()));
    std::cout<<ctx->expression()->getText()<<" = "<< res<<std::endl;
  }
  else if(ctx->CMDSEP())
  {
    //c1=command CMDSEP c2=command
    visitCommand(ctx->c1);
    if(return_interrupt) return antlrcpp::Any();
    visitCommand(ctx->c2);
  }
  else if(ctx->RETURN())
  {
    result = static_cast<int>(visitExpression(ctx->e_return));
    return_interrupt = true;
  }
  return antlrcpp::Any();
}

antlrcpp::Any MiniLFunInterpret::visitFacteur(MiniLParser::FacteurContext* ctx)
{
  path.push_back(ctx->getAltNumber());
  if(ctx->NOMBRE())
  {
    auto nb = ctx->NOMBRE();
    return std::atoi(nb->getText().c_str());
  }
  else if(ctx->var)
  {
    return memory.get(ctx->var->getText());
  }
  else if(ctx->fun_called)
  {
    MiniLFunInterpret childCall(miniL);

    std::vector<int> args;
    for(auto ca: ctx->vars)
      args.push_back(static_cast<int>(visitExpression(ca)));
    return childCall.visitFunction(miniL.getFunction(ctx->fun_called->getText()),args);
  }
  return visitExpression(ctx->pe);
}


antlrcpp::Any MiniLFunInterpret::visitTerme(MiniLParser::TermeContext* ctx)
{
  path.push_back(ctx->getAltNumber());
  if(ctx->MUL())
  {
    return static_cast<int>(visitTerme(ctx->terme()))*static_cast<int>(visitFacteur(ctx->facteur()));
  }
  if(ctx->DIV())
    return static_cast<int>(visitTerme(ctx->terme()))/static_cast<int>(visitFacteur(ctx->facteur()));
  return visitFacteur(ctx->facteur());
}

antlrcpp::Any MiniLFunInterpret::visitExpression(MiniLParser::ExpressionContext* ctx)
{
  path.push_back(ctx->getAltNumber());
  if(ctx->ADD())
  {
    return static_cast<int>(visitExpression(ctx->expression()))+static_cast<int>(visitTerme(ctx->terme()));
  }
  if(ctx->SUB())
  {
    return std::max(static_cast<int>(visitExpression(ctx->expression()))-static_cast<int>(visitTerme(ctx->terme())), 0);
  }
  return visitTerme(ctx->terme());
}

antlrcpp::Any MiniLFunInterpret::visitCondition(MiniLParser::ConditionContext* ctx)
{
  path.push_back(ctx->getAltNumber());
  if(ctx->c)return visitCondition(ctx->c);
  if(ctx->NOT())
  {
    return !static_cast<bool>(visitCondition(ctx->n_cond));
  }
  if(ctx->cop)
  {
    int fop = static_cast<int>(visitComparison_operator(ctx->cop));
    //TODO  | e1=expression cop=comparison_operator e2=expression
    switch(fop){
      case 0:
        return static_cast<int>(visitExpression(ctx->e1)) == static_cast<int>(visitExpression(ctx->e2));
      case 1:
        return static_cast<int>(visitExpression(ctx->e1)) != static_cast<int>(visitExpression(ctx->e2));
      case 2:
        return static_cast<int>(visitExpression(ctx->e1)) <= static_cast<int>(visitExpression(ctx->e2));
      case 3:
        return static_cast<int>(visitExpression(ctx->e1)) < static_cast<int>(visitExpression(ctx->e2));
      case 4:
        return static_cast<int>(visitExpression(ctx->e1)) >= static_cast<int>(visitExpression(ctx->e2));
      case 5:
        return static_cast<int>(visitExpression(ctx->e1)) > static_cast<int>(visitExpression(ctx->e2));
    }
    throw "Unreachable (non iplememted operator)";
  }
  if(ctx->AND())
  {
    return static_cast<bool>(visitCondition(ctx->c1)) && static_cast<bool>(visitCondition(ctx->c2));
  }
  if(ctx->OR())
  {
     return static_cast<bool>(visitCondition(ctx->c1)) || static_cast<bool>(visitCondition(ctx->c2));
  }
  throw "Unreachable (non iplememted condition)";
}

antlrcpp::Any MiniLFunInterpret::visitComparison_operator(MiniLParser::Comparison_operatorContext* ctx)
{
  path.push_back(ctx->getAltNumber());
  if(ctx->EQ())return 0;
  if(ctx->DIFF())return 1;
  if(ctx->INF())return 2;
  if(ctx->INFSTR())return 3;
  if(ctx->GR())return 4;
  if(ctx->GRSTR())return 5;
  throw "Unreachable (non iplememted operator)";
}
