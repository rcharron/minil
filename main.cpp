#include <iostream>
#include <sstream>
#include <antlr4-runtime.h>
#include "Study/StudyBaseVisitor.h"
#include "Study/StudyBaseListener.h"
#include "Study/StudyLexer.h"
#include "Analyse.h"

int main(int argc, char** argv) {
  
    if(argc!=2)
    {
      std::cout<<"Usage : "<<argv[0]<<" filename"<<std::endl;
      return -1;
    }
    std::ifstream stream(argv[1]);
    
    antlr4::ANTLRInputStream input(stream);
    StudyLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    StudyParser parser(&tokens);    
    StudyParser::FileContext* tree = parser.file();
//     std::vector<std::string> ruleNames = parser.getRuleNames();
//     std::cout<<tree->toStringTree(ruleNames)<<std::endl;
    Analyse visitor;
    try{
      auto res = visitor.visitFile(tree);
    }
    catch(std::string str)
    {
      std::cout<<"Erreur : "<<str<<std::endl;
      return -1;
    }
    return 0;
}


