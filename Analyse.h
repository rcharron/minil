#ifndef ANALYSE_H
#define ANALYSE_H

#include "Study/StudyBaseVisitor.h"
#include "Study.h"

class Analyse : public StudyBaseVisitor
{
public:
    antlrcpp::Any visitFile(StudyParser::FileContext* ctx) override;
    antlrcpp::Any visitStudy(StudyParser::StudyContext* ctx) override;
    antlrcpp::Any visitStudy_instruction(StudyParser::Study_instructionContext* ctx) override;
private:
  MiniL miniLStuff;
  std::vector<Study> studies;
};

#endif // ANALYSE_H
