#ifndef STUDY_H
#define STUDY_H
#include <antlr4-runtime.h>
#include "MiniL/MiniLBaseVisitor.h"
#include "MiniL/MiniLBaseListener.h"
#include "MiniL/MiniLLexer.h"
#include "MiniL.h"
#include "FunInterpret.h"
#include "CFGCrafter.h"

struct expe{
  std::vector<int> input;
  int output;
  int expectOutput;
  std::vector<int> path;
};

class Study
{
public:
  Study(std::string name, MiniLParser::FunctionContext * ctx, MiniL& miniL);
  void simplify();
  void printReport();
  void drawCFG(std::string path, std::string format="dot");
  void interpret(std::vector<int> args);
  void interpret(std::vector<int> args, int expect);
  void showPaths(std::string path, std::string format="dot");
private:
  void convert(std::string fileDot, std::string format);
  int computeCC();
  MiniLFunInterpret interpreter;
  CFGCrafter cfgCrafter;
  std::vector<expe> tests;
  MiniLParser::FunctionContext * ctxFun;
  std::string name;
};

#endif // STUDY_H
