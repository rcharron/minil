#ifndef MINIL_H
#define MINIL_H
#include "MiniL/MiniLBaseVisitor.h"
#include "Memory.h"
#include <map>


class MiniL : public MiniLBaseVisitor
{
public:
  virtual antlrcpp::Any visitFile(MiniLParser::FileContext *ctx) override;
  virtual antlrcpp::Any visitExpression(MiniLParser::ExpressionContext *ctx) override;
  virtual antlrcpp::Any visitTerme(MiniLParser::TermeContext *ctx) override;
  virtual antlrcpp::Any visitFacteur(MiniLParser::FacteurContext *ctx) override;
  virtual antlrcpp::Any visitCondition(MiniLParser::ConditionContext *ctx) override;
  virtual antlrcpp::Any visitComparison_operator(MiniLParser::Comparison_operatorContext *ctx) override;
  virtual antlrcpp::Any visitCommand(MiniLParser::CommandContext *ctx) override;
  virtual antlrcpp::Any visitFunction(MiniLParser::FunctionContext *ctx) override;
  MiniLParser::FunctionContext * getFunction(std::string name);
private:
  bool checkFunctionCall();
  std::set<std::pair<std::string,size_t>> called;
  Memory mem;
  std::map<std::string, MiniLParser::FunctionContext * > functions;
  bool return_called=false;
  int altnum=0;
};




#endif // MINIL_H
