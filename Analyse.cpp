#include "Analyse.h"
#include "MiniL/MiniLLexer.h"
#include "MiniL/MiniLParser.h"
#include <fstream>
#include <antlr4-runtime.h>

antlrcpp::Any Analyse::visitFile(StudyParser::FileContext* ctx)
{
  std::string fileMiniL = ctx->p->getText();
  fileMiniL.erase(0,1);
  fileMiniL.pop_back();
  std::ifstream stream(fileMiniL);
  if(!stream)
    throw std::string("Impossible d'ouvrir "+fileMiniL);
    
  antlr4::ANTLRInputStream input(stream);
  MiniLLexer lexer(&input);
  antlr4::CommonTokenStream tokens(&lexer);
  MiniLParser parser(&tokens);    
  MiniLParser::FileContext* tree = parser.file();
  //std::vector<std::string> ruleNames = parser.getRuleNames();
  //std::cout<<tree->toStringTree(ruleNames)<<std::endl;
  
  miniLStuff.visitFile(tree);
  
  for(auto cstudy : ctx->studies)
    visitStudy(cstudy);
  
  for(auto& st : studies)
    st.printReport();
  return antlrcpp::Any();
}

antlrcpp::Any Analyse::visitStudy(StudyParser::StudyContext* ctx)
{
  std::string sname = ctx->study_name->getText();
  std::string funToStudy = ctx->fun_name->getText();
  studies.push_back(Study(sname, miniLStuff.getFunction(funToStudy), miniLStuff));
  if(ctx->EXTENDEDCFG()==nullptr)
    studies.back().simplify();
  for(auto cinstr: ctx->instrs)
    visitStudy_instruction(cinstr);
  return antlrcpp::Any();
}

antlrcpp::Any Analyse::visitStudy_instruction(StudyParser::Study_instructionContext* ctx)
{
  if(ctx->DRAWCFG())
  {
    std::string fout = ctx->PATH()->getText();
    fout.erase(0,1);
    fout.pop_back();
    std::string format;
    if(ctx->DOTCONVERTFORMAT())
    {
      format=ctx->DOTCONVERTFORMAT()->getText();
    }
    studies.back().drawCFG(fout,format);
  }
  else if(ctx->SHOWPATHS())
  {
    std::string fout = ctx->PATH()->getText();
    fout.erase(0,1);
    fout.pop_back();
    std::string format;
    if(ctx->DOTCONVERTFORMAT())
    {
      format=ctx->DOTCONVERTFORMAT()->getText();
    }
    studies.back().showPaths(fout,format);
  }
  else if(ctx->INTERPRET())
  {
    std::vector<int> args;
    for(auto cvar : ctx->vars){
      args.push_back( std::atoi(cvar->getText().c_str()));
    }
    studies.back().interpret(args);
  }
  else if(ctx->TEST())
  {
    std::vector<int> args;
    for(auto cvar : ctx->vars){
      args.push_back( std::atoi(cvar->getText().c_str()));
    }
    int expect=std::atoi(ctx->expect->getText().c_str());
    studies.back().interpret(args, expect);
  }
  return antlrcpp::Any();
}

