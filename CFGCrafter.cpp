#include "CFGCrafter.h"
#include <string>

fragment CFGCrafter::visitCondition(MiniLParser::ConditionContext* ctx)
{
  MetaNode node;
  node.text=ctx->getText();
  node.id=nextIDMetaNode++;
  node.altNodes.insert(ctx->getAltNumber());
  node.conditionNode=true;
  metaNodes[node.id]=node;
  return {node.id, node.id};
}

fragment CFGCrafter::visitCommand(MiniLParser::CommandContext* ctx)
{
  
  if(ctx->LOCAL()||ctx->ATTR()||ctx->PRINT())
  {
    MetaNode node;
    node.text=ctx->getText();
    node.id=nextIDMetaNode++;
    node.altNodes.insert(ctx->getAltNumber());
    node.conditionNode=false;
    metaNodes[node.id]=node;
    return {node.id,node.id};
  }
  else if(ctx->RETURN())
  {
    MetaNode node;
    node.text=ctx->getText();
    node.id=nextIDMetaNode++;
    node.altNodes.insert(ctx->getAltNumber());
    node.conditionNode=false;
    node.next_children.push_back(id_end);
    metaNodes[node.id]=node;
    return {node.id,id_end};
  }
  else if (ctx->IF()&&ctx->ELSE()){
    auto [a,b] = visitCondition(ctx->condition());
    auto[cTa, cTb] = visitCommand(ctx->cmdTrue);
    auto[cFa, cFb] = visitCommand(ctx->cmdFalse);
    MetaNode nodeEnd;
    nodeEnd.text="";
    nodeEnd.id=nextIDMetaNode++;
    nodeEnd.conditionNode=false;
    metaNodes[nodeEnd.id]=nodeEnd;
    metaNodes[b].next_children.push_back(cTa);
    metaNodes[b].next_children.push_back(cFa);
    metaNodes[cTa].prev_children.insert(b);
    metaNodes[cFa].prev_children.insert(b);
    metaNodes[cTb].next_children.push_back(nodeEnd.id);
    metaNodes[cFb].next_children.push_back(nodeEnd.id);
    metaNodes[nodeEnd.id].prev_children.insert(cTb);
    metaNodes[nodeEnd.id].prev_children.insert(cFb);
    return {a,nodeEnd.id};
  }
  else if (ctx->IF()){
    auto [a,b] = visitCondition(ctx->condition());
    auto[cTa, cTb] = visitCommand(ctx->cmdTrue);
    MetaNode nodeEnd;
    nodeEnd.text="";
    nodeEnd.id=nextIDMetaNode++;
    nodeEnd.conditionNode=false;
    metaNodes[nodeEnd.id]=nodeEnd;
    metaNodes[b].next_children.push_back(cTa);
    metaNodes[b].next_children.push_back(nodeEnd.id);
    metaNodes[cTa].prev_children.insert(b);
    metaNodes[cTb].next_children.push_back(nodeEnd.id);
    metaNodes[nodeEnd.id].prev_children.insert(cTb);
    metaNodes[nodeEnd.id].prev_children.insert(b);
    return {a,nodeEnd.id};
  }
  else if(ctx->WHILE()){
    auto [a,b] = visitCondition(ctx->condition());
    auto[ca, cb] = visitCommand(ctx->cmdWhile);
    
    MetaNode nodeEnd;
    nodeEnd.text="";
    nodeEnd.id=nextIDMetaNode++;
    nodeEnd.conditionNode=false;
    metaNodes[nodeEnd.id]=nodeEnd;
    metaNodes[b].next_children.push_back(ca);
    metaNodes[b].next_children.push_back(nodeEnd.id);
    metaNodes[ca].prev_children.insert(b);
    metaNodes[nodeEnd.id].prev_children.insert(b);
    metaNodes[cb].next_children.push_back(b);
    metaNodes[b].prev_children.insert(cb);
    return {a,nodeEnd.id};
  }
  else if(ctx->CMDSEP())
  {
    //c1=command CMDSEP c2=command
    auto [c1a,c1b] = visitCommand(ctx->c1);
    auto [c2a,c2b] = visitCommand(ctx->c2);
    metaNodes[c1b].next_children.push_back(c2a);
    metaNodes[c2a].prev_children.insert(c1b);
    return {c1a, c2b};
  }
  throw "Should not reach this point";
}

void CFGCrafter::visitFunction(MiniLParser::FunctionContext* ctx)
{
  nextIDMetaNode=0;
  metaNodes.clear();
  funName=ctx->fun_name->getText();
  args.clear();
  for(auto ctxa : ctx->vars)
    args.push_back(ctxa->getText());
  auto [a,b] = visitCommand(ctx->command());
  metaNodes[a].prev_children.insert(id_start);
  metaNodes[id_start].next_children.push_back(a);
  metaNodes[b].next_children.push_back(id_end);
  metaNodes[id_end].prev_children.insert(b);
  std::vector<int> workset = metaNodes[id_end].next_children;
  metaNodes[id_end].next_children.clear();
  for(int i : workset)
    metaNodes[i].prev_children.erase(id_end);
  recomputeAltToMetaID();
  recomputeStats();
}

void CFGCrafter::simplifyCFG()
{
  std::set<int> nodesToTreat;
  for(auto node : metaNodes){
    if(node.second.prev_children.size()==1)
    {
      if(!node.second.conditionNode)
        nodesToTreat.insert(node.first);
    }
  }
  for(int nID: nodesToTreat)
  {
    MetaNode& mn = metaNodes.at(nID);
    int pID = *mn.prev_children.begin();
    if(pID==id_start) 
      continue;
    MetaNode& parent = metaNodes.at(pID);
    if(parent.next_children.size()>1)
      continue;
    if(parent.text=="")
      parent.text=mn.text;
    else 
      parent.text+="\\l"+mn.text;
      
    for(int an : mn.altNodes)
      parent.altNodes.insert(an);
    parent.next_children.clear();
    for(int nN : mn.next_children)
    {
      parent.next_children.push_back(nN);
      metaNodes[nN].prev_children.erase(nID);
      metaNodes[nN].prev_children.insert(pID);
    }
    metaNodes.erase(nID);
  }
  recomputeAltToMetaID();
  recomputeStats();
}

std::string ReplaceAll(std::string str, const std::string& from, const std::string& to) {
  size_t start_pos = 0;
  while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
  }
  return str;
}

std::string treatForDot(std::string str) {
  str=ReplaceAll(str,"&","&amp;");
  str=ReplaceAll(str,"|","\\|");
  str=ReplaceAll(str,">","&gt;");
  str=ReplaceAll(str,"<","&lt;");
  return str;
}

std::string CFGCrafter::Namer(int id)
{
    if(id==id_start)return "NStart";
    if(id==id_end)return "NEnd";
    std::stringstream ss;
    ss<<"N"<<id;
    return ss.str();
}


void CFGCrafter::printDot(std::string filename)
{
  std::ofstream os(filename);
  os<<"digraph \""<<funName<<"\" {"<<std::endl;
  os<<"\tlabel=\""<<funName<<"\";"<<std::endl;
  for(auto mnk :metaNodes)
  {
    MetaNode mn=mnk.second;
    if(mnk.first==id_start)
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=record,label=\"{"<<funName<<"(";
      if(args.size()>0)
        os<<args[0];
      for(size_t i=1;i<args.size();i++)
        os<<", "<<args[i];
      os<<")}\"];"<<std::endl;
    for(int nN : mn.next_children)
      os<<"\t"<<Namer(mnk.first)<<" -> "<<Namer(nN)<<";"<<std::endl;
    }
    else if(mnk.first==id_end)
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=circle,label=\"{Fin}\"];"<<std::endl;
    }
    else if(mn.conditionNode)
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=record,label=\""<<mnk.first<<"|{"<<treatForDot(mn.text)<<"|{<s0>Vrai|<s1>Faux}}\"];"<<std::endl;
      os<<"\t"<<Namer(mnk.first)<<":s0 -> "<<Namer(mn.next_children[0])<<";"<<std::endl;
      os<<"\t"<<Namer(mnk.first)<<":s1 -> "<<Namer(mn.next_children[1])<<";"<<std::endl;
    }
    else
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=record,label=\""<<mnk.first<<"|{ "<<treatForDot(mn.text)<<"}\"];"<<std::endl;
      if(!mn.next_children.empty())os<<"\t"<<Namer(mnk.first)<<" -> "<<Namer(mn.next_children[0])<<";"<<std::endl;
    }
  }
  
  os<<"}"<<std::endl;
}

void CFGCrafter::printDotColorArc(std::ostream& os, int n1, int n2, bool colorArc, bool colorPositif, bool colorNegatif, std::string n1_suffix)
{
  std::string colorTxt;
  if(colorArc)
  {
    if(arcsColorGreen.count({n1,n2})>0)
    {
      if(colorPositif)colorTxt="[color=\"green\"]";
    }
    else
    {
      if(colorNegatif)colorTxt="[color=\"red\"]";
    }
  }
  os << "\t" << Namer(n1) << n1_suffix << " -> " << Namer(n2) << colorTxt << ";" << std::endl;
}


void CFGCrafter::printDotColor(std::string filename, bool colorNodes, bool colorArc, bool colorPositif, bool colorNegatif)
{
  std::ofstream os(filename);
  os<<"digraph \""<<funName<<"\" {"<<std::endl;
  os<<"\tlabel=\""<<funName<<"\";"<<std::endl;
  for(auto mnk :metaNodes)
  {
    MetaNode mn=mnk.second;
    std::string colorTxt;
    if(colorNodes){
      if(nodesColorGreen.count(mnk.first)>0){
        if(colorPositif)colorTxt=",color=\"green\"";
      }
      else
        if(colorNegatif)colorTxt=",color=\"red\"";
    }
    if(mnk.first==id_start)
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=record"<<colorTxt<<",label=\"{"<<funName<<"(";
      if(args.size()>0)
        os<<args[0];
      for(size_t i=1;i<args.size();i++)
        os<<", "<<args[i];
      os<<")}\"];"<<std::endl;
    for(int nN : mn.next_children)
      printDotColorArc(os, mnk.first, nN,colorArc, colorPositif, colorNegatif);
    }
    else if(mnk.first==id_end)
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=circle"<<colorTxt<<",label=\"{Fin}\"];"<<std::endl;
    }
    else if(mn.conditionNode)
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=record"<<colorTxt<<",label=\""<<mnk.first<<"|{"<<treatForDot(mn.text)<<"|{<s0>Vrai|<s1>Faux}}\"];"<<std::endl;
      printDotColorArc(os, mnk.first, mn.next_children[0], colorArc, colorPositif, colorNegatif, ":s0");
      printDotColorArc(os, mnk.first, mn.next_children[1], colorArc, colorPositif, colorNegatif, ":s1");
    }
    else
    {
      os<<"\t"<<Namer(mnk.first)<<" [shape=record"<<colorTxt<<",label=\""<<mnk.first<<"|{ "<<treatForDot(mn.text)<<"}\"];"<<std::endl;
      if(!mn.next_children.empty())printDotColorArc(os, mnk.first, mn.next_children[0], colorArc, colorPositif, colorNegatif);
    }
  }
  
  os<<"}"<<std::endl;
}

void CFGCrafter::recomputeAltToMetaID()
{
  altToMetaID.clear();
  altToMetaAddendumID.clear();
  for(auto kmn : metaNodes){
    for(int an : kmn.second.altNodes)
      altToMetaID[an]=kmn.first;
    if(kmn.second.altNodes.empty() && kmn.first!=id_start)
    {
      if(kmn.second.next_children.size()==1)
        altToMetaAddendumID[kmn.second.next_children[0]]=kmn.first;
    }
  }
}

void CFGCrafter::recomputeStats()
{
  cyclomaticComplexity=1;
  countNodes=0;
  countArc=0;
  ill=false;
  nodeSet.clear();
  arcSet.clear();
  for(auto kmn : metaNodes){
    MetaNode& mn =kmn.second;
    if(mn.conditionNode)
      cyclomaticComplexity++;
    countNodes++;
    nodeSet.insert(kmn.first);
    countArc+=mn.next_children.size();
    for(int i : mn.next_children)
      arcSet.insert({kmn.first, i});
    if(mn.prev_children.empty())
      ill|=(kmn.first!=id_start);
  }
  arcsColorGreen.clear();
  nodesColorGreen.clear();
}

std::vector<int> CFGCrafter::altToCFGPath(std::vector<int> path)
{
  std::vector<int> res;
  res.push_back(id_start);
  for(int n : path)
  {
    auto it=altToMetaID.find(n);
    if(it!=altToMetaID.end()){
      if(res.back()!=it->second){
        auto ita = altToMetaAddendumID.find(it->second);
        if(ita!=altToMetaAddendumID.end())
          res.push_back(ita->second);
        res.push_back(it->second);
      }
    }
  }
  res.push_back(id_end);
  return res;
}

int CFGCrafter::getCountNodes()
{
  return countNodes;
}

int CFGCrafter::getCountArc()
{
  return countArc;
}

std::set<int> CFGCrafter::getNodeSet()
{
  return nodeSet;
}

int CFGCrafter::getCyclomaticComplexity()
{
  return cyclomaticComplexity;
}

std::set<std::pair<int, int> > CFGCrafter::getArcSet()
{
  return arcSet;
}

std::set<int> CFGCrafter::getNodesCovered()
{
  return nodesColorGreen;
}

std::set<std::pair<int, int> > CFGCrafter::getArcsCovered()
{
  return arcsColorGreen;
}

void CFGCrafter::addTest(std::vector<int> path)
{
  size_t t=path.size();
  nodesColorGreen.insert(path[0]);
  for(size_t i=1;i<t;i++)
  {
    nodesColorGreen.insert(path[i]);
    arcsColorGreen.insert({path[i-1],path[i]});
  }
}
