#include "MiniL.h"
#include <cstdlib>
#include <cmath>
#include <functional>


antlrcpp::Any MiniL::visitFacteur(MiniLParser::FacteurContext* ctx)
{
  ctx->setAltNumber(altnum++);
  if(ctx->pe)
  {
    visitExpression(ctx->pe);
  }
  else if(ctx->var)
  {
    mem.get(ctx->var->getText());
  }
  else if(ctx->fun_called)
  {
    called.insert({ctx->fun_called->getText(), ctx->vars.size()});
  }
  return antlrcpp::Any();
}


antlrcpp::Any MiniL::visitTerme(MiniLParser::TermeContext* ctx)
{
  ctx->setAltNumber(altnum++);
  if(ctx->terme()) visitTerme(ctx->terme());
  if(ctx->facteur())visitFacteur(ctx->facteur());
  return antlrcpp::Any();
}

antlrcpp::Any MiniL::visitExpression(MiniLParser::ExpressionContext* ctx)
{
  ctx->setAltNumber(altnum++);
  if(ctx->expression())visitExpression(ctx->expression());
  if(ctx->terme())visitTerme(ctx->terme());
  return antlrcpp::Any();
}

antlrcpp::Any MiniL::visitComparison_operator(MiniLParser::Comparison_operatorContext* ctx)
{
  ctx->setAltNumber(altnum++);
  return antlrcpp::Any();
}

antlrcpp::Any MiniL::visitCondition(MiniLParser::ConditionContext* ctx)
{
  ctx->setAltNumber(altnum++);
  if(ctx->c)return visitCondition(ctx->c);
  if(ctx->NOT())visitCondition(ctx->n_cond);
  if(ctx->cop)
  {
    visitExpression(ctx->e1);
    visitExpression(ctx->e2);
  }
  if(ctx->AND()||ctx->OR())
  {
    visitCondition(ctx->c1);
    visitCondition(ctx->c2);
  }
  return antlrcpp::Any();
}

antlrcpp::Any MiniL::visitCommand(MiniLParser::CommandContext* ctx)
{
  ctx->setAltNumber(altnum++);
  if(ctx->LOCAL())
  {
    if(ctx->expression()){
      visitExpression(ctx->expression());
      mem.declare(ctx->VARIABLE()->getText(),0);
    }
    else
      mem.declare(ctx->VARIABLE()->getText());
  }
  else if (ctx->ATTR()){
    visitExpression(ctx->expression());
    mem.set(ctx->VARIABLE()->getText(),0);
  }
  else if (ctx->IF()){
    visitCondition(ctx->condition());
    Memory mem_backup = mem;
    bool ret0=return_called;
    visitCommand(ctx->cmdTrue);
    bool ret1=return_called;
    bool ret2=ret0;
    if(ctx->cmdFalse){
      Memory memTrue = mem;
      return_called=ret0;
      mem = mem_backup;
      visitCommand(ctx->cmdFalse);
      ret2=return_called;
      Memory memFalse = mem;
      mem=mem_backup;
      for(auto& [key, val] : mem)
      {
        if(memTrue.initialized(key)&&memFalse.initialized(key))
          val=0;
      }
    }
    else
      mem=mem_backup;
    return_called=ret1&ret2;
  }
  else if(ctx->WHILE()){
    bool ret1=return_called;
    visitCondition(ctx->condition());
    Memory mem_backup = mem;
    visitCommand(ctx->cmdWhile);
    mem = mem_backup;
    return_called=ret1;
  }
  else if(ctx->PRINT()){
    visitExpression(ctx->expression());
  }
  else if(ctx->CMDSEP())
  {
    visitCommand(ctx->c1);
    visitCommand(ctx->c2);
  }
  else if(ctx->RETURN())
  {
    visitExpression(ctx->e_return);
    return_called = true;
  }
  return antlrcpp::Any();
}

antlrcpp::Any MiniL::visitFunction(MiniLParser::FunctionContext* ctx)
{
  std::string funName = ctx->fun_name->getText();
  if(functions.find(funName)!=functions.end())
    throw std::string("La fonction "+ funName+" est définie plusieurs fois.");
  mem.clear();
  return_called=false;
  for(size_t i=0 ;i < ctx->vars.size();i++)
  {
    mem.declare(ctx->vars[i]->getText(), 0);
  }
  visitCommand(ctx->command());
  if(!return_called)
    throw std::string("L'instruction return n'est pas forcément atteinte ou c'est trop complexe à décider");
  
  functions.insert({funName, ctx});
  return antlrcpp::Any();
}

antlrcpp::Any MiniL::visitFile(MiniLParser::FileContext* ctx)
{
  for(auto f : ctx->funs)
  visitFunction(f);
  checkFunctionCall();
  return antlrcpp::Any();
}

MiniLParser::FunctionContext * MiniL::getFunction(std::string name)
{
  auto it = functions.find(name);
  if(it == functions.end()) 
    throw std::string("La fonction "+name+" n'existe pas.");
  return it->second;
}

bool MiniL::checkFunctionCall()
{
  for(auto [fn, ai] : called)
  {
    if(functions.find(fn)==functions.end())
      throw std::string("Appel à la fonction "+fn+" non définie.");
    if(ai!= functions.at(fn)->vars.size()){
      std::stringstream ss;
      ss<<"Appel à la fonction "<<fn<<" avec un nombre inconsistent d'arguments : "<<ai<<", attendus : "<<functions.at(fn)->vars.size()<<".";
      throw std::string(ss.str());
    }
  }
  return true;
}
