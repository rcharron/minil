#include "Memory.h"


void Memory::declare(std::string v)
{
  declare(v, -1);
}

void Memory::declare(std::string v, int val)
{
  if(vars.find(v)!=vars.end())throw std::string("redéclaration d'une variable");
  vars.insert({v,val});
}

bool Memory::exist(std::string v)
{
  return vars.find(v)!=vars.end();
}

bool Memory::initialized(std::string v)
{
  if(!exist(v))throw std::string("Réference à une variable non définie");
  return vars[v]>=0;
}


int Memory::get(std::string v)
{
  if(!exist(v))throw std::string("Réference à une variable non définie");
  if(vars[v]<0)throw std::string("Utilisation d'une variable non initialisée");
  return vars.at(v);
}

void Memory::set(std::string v, int value)
{
  if(!exist(v))throw std::string("Réference à une variable non définie");
  vars.at(v)=std::max(0,value);
}

void Memory::clear()
{
  vars.clear();
}

std::map<std::string, int>::iterator Memory::begin()
{
  return vars.begin();
}

std::map<std::string, int>::iterator Memory::end()
{
  return vars.end();
}

