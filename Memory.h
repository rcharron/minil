#ifndef MEMORY_H
#define MEMORY_H

#include <map>
#include <string>

class Memory{
public:
  void declare(std::string v);
  void declare(std::string v, int val);
  bool exist(std::string v);
  bool initialized(std::string v);
  int get(std::string v);
  void set(std::string v, int value);
  void clear();
  std::map<std::string, int>::iterator begin();
  std::map<std::string, int>::iterator end();
private:
  std::map<std::string, int> vars;
};


#endif
