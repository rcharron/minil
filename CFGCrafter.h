#ifndef CFGCRAFTER_H
#define CFGCRAFTER_H

#include "MiniL/MiniLBaseVisitor.h"

class MetaNode{
public:
  int id;
  bool conditionNode;
  std::string text;
  std::set<int> altNodes;
  std::set<int> prev_children;
  std::vector<int> next_children;
};

typedef std::pair<int, int> fragment;

class CFGCrafter
{
public:
    void visitFunction(MiniLParser::FunctionContext* ctx);
    void simplifyCFG();
    void printDot(std::string filename);
    void printDotColor(std::string filename, bool colorNodes, bool colorArc, bool colorPositif, bool colorNegatif);
    std::vector<int> altToCFGPath(std::vector<int> path);
    int getCyclomaticComplexity();
    int getCountNodes();
    int getCountArc();
    std::set<int> getNodeSet();
    std::set<std::pair<int,int>> getArcSet();
    std::set<std::pair<int,int>> getArcsCovered();
    std::set<int> getNodesCovered();
    void addTest(std::vector<int>path);
private:
  void printDotColorArc(std::ostream& os, int n1, int n2,bool colorArc, bool colorPositif, bool colorNegatif, std::string n1_suffix="");
  std::string Namer(int);
    fragment visitCondition(MiniLParser::ConditionContext* ctx);
    fragment visitCommand(MiniLParser::CommandContext* ctx);
    void recomputeAltToMetaID();
    void recomputeStats();
    
    std::map<int, MetaNode> metaNodes;
    const int id_start = -1;
    const int id_end = -2;
    int nextIDMetaNode = 0;
    std::string funName;
    std::vector<std::string> args;
    std::map<int, int> altToMetaID;
    std::map<int, int> altToMetaAddendumID;
    int cyclomaticComplexity=0;
    int countNodes=0;
    int countArc=0;
    bool ill=false;
    std::set<int> nodeSet;
    std::set<std::pair<int,int>> arcSet;
    std::set<std::pair<int,int>> arcsColorGreen;
    std::set<int> nodesColorGreen;
};

#endif // CFGCRAFTER_H
