#ifndef FUN_INTERPRET_H
#define FUN_INTERPRET_H


#include "MiniL/MiniLBaseVisitor.h"
#include "Memory.h"
#include "MiniL.h"

class MiniLFunInterpret : private MiniLBaseVisitor
{
private:
  virtual antlrcpp::Any visitExpression(MiniLParser::ExpressionContext *ctx) override;
  virtual antlrcpp::Any visitTerme(MiniLParser::TermeContext *ctx) override;
  virtual antlrcpp::Any visitFacteur(MiniLParser::FacteurContext *ctx) override;
  virtual antlrcpp::Any visitCondition(MiniLParser::ConditionContext *ctx) override;
  virtual antlrcpp::Any visitComparison_operator(MiniLParser::Comparison_operatorContext *ctx) override;
  virtual antlrcpp::Any visitCommand(MiniLParser::CommandContext *ctx) override;
  virtual antlrcpp::Any visitFunction(MiniLParser::FunctionContext *ctx) override;
public:
  MiniLFunInterpret(MiniL& miniL);
  int visitFunction(MiniLParser::FunctionContext *ctx, std::vector<int> inputArgs);
  std::vector<int> getPath();
private:
  MiniL& miniL;
  std::vector<int> path;
  Memory memory;
  bool return_interrupt=false;
  int result;
};

#endif
