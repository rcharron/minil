grammar Study;

file : FROM '(' p=PATH ')' (studies+=study)* EOF;

study : STUDY study_name = VARIABLE WITH '(' fun_name=VARIABLE ')' ( '_' EXTENDEDCFG )? (instrs+=study_instruction)* END;

study_instruction: DRAWCFG '(' PATH ')' ( '_' DOTCONVERTFORMAT)?
                | INTERPRET'('(vars+=NUMBER','?)*')'
                | TEST '('(vars+=NUMBER','?)*')''='expect=NUMBER
                | SHOWPATHS '(' PATH ')' ( '_' DOTCONVERTFORMAT )?
                ;


PATH     : '"'[a-zA-Z0-9_.]+'"';

FROM       : 'from';
WITH       : 'with';
STUDY      : 'study';
EXTENDEDCFG: 'extendedCFG';
DRAWCFG    : 'drawCFG';
INTERPRET  : 'interpret';
END        : 'end';
TEST       : 'test';
SHOWPATHS  : 'showPaths';

DOTCONVERTFORMAT     : 'pdf'|'ps';


NUMBER   :  [0-9]+;
VARIABLE : [a-zA-Z][a-zA-Z0-9_]*;

WHITESPACE  : (' '|'\t'|'\n'|'\r') -> skip ;

