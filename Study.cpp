#include "Study.h"
#include <stdlib.h> 
#include <Eigen/Dense>

Study::Study(std::string name, MiniLParser::FunctionContext* ctx, MiniL& miniL) : interpreter(miniL) ,ctxFun(ctx), name(name)
{
  //interpret.visitFunction(ctx);
  cfgCrafter.visitFunction(ctx);
}

void Study::simplify()
{
  cfgCrafter.simplifyCFG();
}

void Study::interpret(std::vector<int> args)
{
  interpreter.visitFunction(ctxFun,args);
}

void Study::drawCFG(std::string path, std::string format)
{
  cfgCrafter.printDot(path);
  if(format!="dot")convert(path,format);
}

void Study::interpret(std::vector<int> args, int expect)
{
  expe ne;
  ne.input = args;
  ne.expectOutput = expect;
  ne.output = interpreter.visitFunction(ctxFun, args);
  auto path = interpreter.getPath();
  ne.path = cfgCrafter.altToCFGPath(path);
  cfgCrafter.addTest(ne.path);
  tests.push_back(ne);
}

void Study::showPaths(std::string path, std::string format)
{
  cfgCrafter.printDotColor(path,true,true,true,true);
  if(format!="dot")convert(path,format);
}

void Study::convert(std::string fileDot, std::string format)
{
  if(format=="")return;
  std::string fileOut=fileDot;
  size_t t=fileOut.size();
  if(t>4)
  {
    if(fileOut[t-4]=='.'&&fileOut[t-3]=='d'&&fileOut[t-2]=='o'&&fileOut[t-1]=='t')
    {
      for(int i:{0,1,2,3})fileOut.pop_back();
    }
  }
  fileOut+=".";
  fileOut+=format;
  
  std::stringstream sys;
  sys<<"dot -T"<<format<<" "<<fileDot<<" -o "<<fileOut;
  system(sys.str().c_str());
}

int Study::computeCC()
{
  std::set<std::pair<int,int>> arcSet = cfgCrafter.getArcSet();
  std::map<std::pair<int,int>, int> arcID;
  int ID=0;
  for(std::pair<int,int> arc: arcSet)
  {
    arcID[arc]=ID;
    ID++;
  }
  int nbTests=tests.size();
  Eigen::MatrixXd mat;
  mat.resize(ID,nbTests);
  mat.setZero();
  for(int i=0;i<nbTests;i++)
  {
    auto p = tests[i].path;
    int plength=p.size();
    for(int pi=1;pi<plength;pi++)
    {
      mat(arcID.at({p[pi-1],p[pi]}),i)+=1.;
    }
  }
  return mat.colPivHouseholderQr().rank();
}


void Study::printReport()
{
  std::cout<<name<<" : "<<std::endl;
  int success=0;
  for(expe& e: tests){
    if(e.expectOutput==e.output)success++;
  }
  std::cout<<success<<"/"<<tests.size()<<" tests ont passés"<<std::endl;
  std::cout<<"Couverture par noeud : "<<(cfgCrafter.getCountNodes()==cfgCrafter.getNodesCovered().size()?"OK":"Non")<<std::endl;
  std::cout<<"Couverture par arc : "<<(cfgCrafter.getCountArc()==cfgCrafter.getArcsCovered().size()?"OK":"Non")<<std::endl;
  int cCC = computeCC();
  int gCC = cfgCrafter.getCyclomaticComplexity();
  std::cout<<"Complexité cyclomatique : "<<cCC<<'/'<<gCC<< ' '<<(cCC==gCC?"OK":"Non")<<std::endl;
  
  std::cout<<std::endl;
}
