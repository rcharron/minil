grammar MiniL;

file : (funs+=function)* EOF;

expression : expression ADD terme
           | expression SUB terme
           | terme
           ;
terme      : terme MUL facteur
           | terme DIV facteur
           | facteur
           ;
facteur    : '('pe=expression')'
           | NOMBRE
           | fun_called=VARIABLE '('(vars+=expression','?)*')'
           | var=VARIABLE
           ;
           
condition  : '('c=condition')'
           | NOT n_cond = condition
           | e1=expression cop=comparison_operator e2=expression
           | c1=condition AND c2=condition
           | c1=condition OR c2=condition
           ;
comparison_operator : EQ | DIFF | INF | INFSTR | GR | GRSTR ;

command : LOCAL VARIABLE (ATTR expression)?
        | VARIABLE ATTR expression
        | IF condition THEN cmdTrue=command (ELSE cmdFalse=command)? END
        | WHILE condition DO cmdWhile=command END
        | PRINT expression
        | c1=command CMDSEP c2=command
        | RETURN e_return=expression
        ;
        
function : FUNCTION fun_name=VARIABLE '('(vars+=VARIABLE','?)*')'command END;


ADD        : '+';
SUB        : '-';
MUL        : '*';
DIV        : '/';
NOMBRE     :  [0-9]+;
EQ         : '==';
DIFF       : '!=';
INF        : '<=';
INFSTR     : '<';
GR         : '>=';
GRSTR      : '>';
AND        : '&&';
OR         : '||';
NOT        : '!';
ATTR       : ':=';

WHILE      : 'while';
DO         : 'do';
IF         : 'if';
THEN       : 'then';
ELSE       : 'else';
END        : 'end';
FUNCTION   : 'fun';
LOCAL      : 'local';
RETURN     : 'return';
CMDSEP     : ';';
PRINT      : 'print';

VARIABLE : [a-zA-Z][a-zA-Z0-9_]*;

WHITESPACE  : (' '|'\t'|'\n'|'\r') -> skip ;

